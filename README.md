## Pre-requisitos
- Spring Boot 2.7.3
- Spring 5.3.22
- Tomcat embed 9.0.65
- YugabyteDB
- Maven 3.8.6
- Java 8

## Construir proyecto
```
mvn clean install
```
