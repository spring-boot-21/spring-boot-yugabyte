package com.zademy.yugabyte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YugabyteApplication {

	public static void main(String[] args) {
		SpringApplication.run(YugabyteApplication.class, args);
	}

}
