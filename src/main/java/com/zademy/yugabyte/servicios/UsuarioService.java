/**
 * 
 */
package com.zademy.yugabyte.servicios;

import java.util.List;

import com.zademy.yugabyte.persistencia.entidades.Usuario;

/**
 * The Interface UsuarioService.
 *
 * @author Sadot
 */
public interface UsuarioService {

	/**
	 * Find all.
	 *
	 * @return the list
	 */
	List<Usuario> findAll();

}
