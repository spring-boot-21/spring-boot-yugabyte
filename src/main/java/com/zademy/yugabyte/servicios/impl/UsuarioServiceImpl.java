/**
 * 
 */
package com.zademy.yugabyte.servicios.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zademy.yugabyte.persistencia.entidades.Usuario;
import com.zademy.yugabyte.persistencia.repository.UsuarioRepository;
import com.zademy.yugabyte.servicios.UsuarioService;

/**
 * The Class UsuarioServiceImpl.
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {

	/** The repository usuario repository. */
	@Autowired
	private UsuarioRepository repositoryUsuarioRepository;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.zademy.yugabyte.servicios.UsuarioService#findAll()
	 */
	@Override
	public List<Usuario> findAll() {

		return repositoryUsuarioRepository.findAll();
	}

}
