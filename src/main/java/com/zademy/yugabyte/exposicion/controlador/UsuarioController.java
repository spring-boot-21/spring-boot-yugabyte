/**
 * 
 */
package com.zademy.yugabyte.exposicion.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zademy.yugabyte.persistencia.entidades.Usuario;
import com.zademy.yugabyte.servicios.UsuarioService;

/**
 * The Class UsuarioController.
 */
@RestController
public class UsuarioController {

	/** The usuario service. */
	@Autowired
	private UsuarioService usuarioService;

	/**
	 * Obtener usuarios.
	 *
	 * @return the list
	 */
	@GetMapping("/")
	public List<Usuario> obtenerUsuarios() {

		return usuarioService.findAll();

	}

}
