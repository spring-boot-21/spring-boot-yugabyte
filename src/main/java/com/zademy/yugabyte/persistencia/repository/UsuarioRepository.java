/**
 * 
 */
package com.zademy.yugabyte.persistencia.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zademy.yugabyte.persistencia.entidades.Usuario;

/**
 * The Interface UsuarioRepository.
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

}
