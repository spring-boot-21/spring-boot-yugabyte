/**
 * 
 */
package com.zademy.yugabyte.persistencia.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class Usuario.
 */
@Entity
@Table(name = "usuarios")
public class Usuario {

	/** The id usuario. */
	@Id
	@Column(name = "id_usuario")
	private Integer idUsuario;

	/** The nombre. */
	@Column(name = "nombre")
	private String nombre;

	/** The apellido. */
	@Column(name = "apellido")
	private String apellido;

	/**
	 * Instantiates a new usuario.
	 */
	public Usuario() {
		super();
	}

	/**
	 * Devuelve el valor de la propiedad apellido.
	 *
	 * @return apellido
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * Devuelve el valor de la propiedad idUsuario.
	 *
	 * @return idUsuario
	 */
	public Integer getIdUsuario() {
		return idUsuario;
	}

	/**
	 * Devuelve el valor de la propiedad nombre.
	 *
	 * @return nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Establece el valor de la propiedad apellido.
	 *
	 * @param apellido el apellido a establecer
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * Establece el valor de la propiedad idUsuario.
	 *
	 * @param idUsuario el idUsuario a establecer
	 */
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	/**
	 * Establece el valor de la propiedad nombre.
	 *
	 * @param nombre el nombre a establecer
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Usuario [idUsuario=");
		builder.append(idUsuario);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", apellido=");
		builder.append(apellido);
		builder.append("]");
		return builder.toString();
	}

}
